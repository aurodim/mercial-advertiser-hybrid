import { Component, ElementRef, ViewChild } from "@angular/core";
import { LoginComponent } from "../login/login.component";
import { Nav, TextInput, App, ModalController } from "ionic-angular";
import { APIService } from "../../../app/auth/api.service";
import { AuthService } from "../../../app/auth/auth.service";
import { AuthComponent } from "../../Auth/auth.component";
import { NgForm } from "@angular/forms";
import {} from '@types/googlemaps';
import { LoaderService } from "../../../app/loader/loader.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { LegalViewerComponent } from "../../Legal/legal.viewer.component";

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html'
})
export class RegisterComponent {
  login = LoginComponent;
  @ViewChild('registerForm') registerForm : NgForm;
  franchise_place = null;

  constructor(private app : App, private modalController : ModalController, private nav:Nav, private loaderService : LoaderService, private api : APIService, private authService : AuthService, private snackbarService : SnackbarService) {}

  ionViewDidLoad() {
    document.getElementsByClassName('legal-tos')[0].addEventListener('click', () => {
      this.onLegalView('terms-of-use');
    });
    document.getElementsByClassName('legal-pp')[0].addEventListener('click', () => {
      this.onLegalView('privacy-policy');
    });
    document.getElementsByClassName('legal-dis')[0].addEventListener('click', () => {
      this.onLegalView('disclaimer')
    });
  }

  onLegalView(doc) {
    this.app.getRootNav().push(LegalViewerComponent, {type: doc});
  }

  onRegisterSubmit(registerForm : NgForm) {
    if (registerForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.register(registerForm.value).subscribe(
      (response : any) => {
        if (response._body._m === "_emailinuse") {
          this.loaderService.handleLoader(false);
          registerForm.controls['email'].setErrors({'inuse' : true});
          this.snackbarService.handleError({"message" : "email_inuse", "isError" : true});
          return;
        }

        this.authService.authenticate(response._body, ()=>{
          this.app.getRootNav().push(AuthComponent, {firstTime : true}).then(() => {
            this.app.getRootNav().remove(0, 1);
          });
        });
        registerForm.resetForm();
        this.loaderService.handleLoader(false);
      },
      error => {
        this.snackbarService.handleError({"message" : error.status + " " + error.statusText, "isError" : true, raw : true});
        this.loaderService.handleLoader(false);
      }
    );
  }

  ionViewWillEnter() {
     this.nav.swipeBackEnabled = false;
  }

  ionViewWillLeave() {
      this.nav.swipeBackEnabled = true;
  }
}
