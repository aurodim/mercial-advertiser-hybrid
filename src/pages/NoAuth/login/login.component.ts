import { Component, ViewChild, ElementRef, OnInit } from "@angular/core";
import { RegisterComponent } from "../register/register.component";
import { NavController, App } from "ionic-angular";
import { NgForm } from "@angular/forms";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { AuthComponent } from "../../Auth/auth.component";
import { AuthService } from "../../../app/auth/auth.service";
import { APIService } from "../../../app/auth/api.service";

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  formatted : string;
  register = RegisterComponent;

  constructor(public navCtrl: NavController, private app : App, private authService : AuthService, private snackbarService : SnackbarService, private loaderService : LoaderService, private api : APIService) { }

  ngOnInit() {}

  onLogin(loginForm : NgForm) {
    if (loginForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.login(loginForm.value).subscribe(
      (response : any) => {
        if (response._body._m === "_emailinvalid") {
          loginForm.controls['email'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "email_unrecognized", "isError" : true});
        } else if (response._body._m === "_passinvalid") {
          loginForm.controls['accesskey'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "incorrect_password", "isError" : true});
        } else if (response._body._m === "_accountlocked") {
          loginForm.controls['email'].setErrors({'invalid' : true});
          loginForm.controls['accesskey'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "account_locked", "isError" : true});
        } else {
          this.authService.authenticate(response._body, ()=>{
            this.app.getRootNav().push(AuthComponent).then(() => {
              this.loaderService.handleLoader(false);
              this.app.getRootNav().remove(0, 1);
            });
          });
          loginForm.resetForm();
          return;
        }
        this.loaderService.handleLoader(false);
      },
      error => {
        this.snackbarService.handleError({"message" : error.status + " " + error.statusText, "isError" : true, raw : true});
        this.loaderService.handleLoader(false);
      }
    );
  }

  onKeyChange($event) {
    if (!$event) {
      return;
    }
    let formattedkey = $event.split('-').join('');
    if (formattedkey.length > 0) {
      formattedkey = formattedkey.match(new RegExp('.{1,4}', 'g')).join('-').match(new RegExp('.{1,7}', 'g')).join('-');
    }
    this.formatted = formattedkey;
  }
}
