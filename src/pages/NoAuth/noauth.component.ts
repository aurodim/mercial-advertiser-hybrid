import { Component } from "@angular/core";
import { NavController, Platform } from "ionic-angular";
import { LoginComponent } from "./login/login.component";
import { AuthService } from "../../app/auth/auth.service";
import { LandingComponent } from "./landing/landing.component";

@Component({
  selector: 'app-no-auth',
  templateUrl: 'noauth.component.html'
})
export class NoAuthComponent {
  rootPage:any = LandingComponent;

  constructor(public navCtrl: NavController, private platform : Platform, private authService : AuthService) {
    platform.ready().then(() => {
        if (localStorage.getItem("FIRST_TIME") != "FALSE") {
          localStorage.setItem("FIRST_TIME", "FALSE");
        } else {
          this.rootPage = LoginComponent;
        }
     });
  }

  ionViewCanEnter() {
      !this.authService.isAuthenticated()
  }

  ionViewCanLeave() {
      this.authService.isAuthenticated()
  }
}
