import { Component } from "@angular/core";
import "../../../../node_modules/chart.js/src/chart.js";
import { TranslateService } from "@ngx-translate/core";
import { AdvertiserMercial } from "../mercials/mercial/advertiser-mercial.model.js";
import { APIService } from "../../../app/auth/api.service";
import { AuthService } from "../../../app/auth/auth.service";
import { LoaderService } from "../../../app/loader/loader.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent {
  posts : number;
  balance : number = 0;
  secondsViewed : number;
  views : number;
  ready : boolean = false;
  topMercial : string = "";
  bestMercials : AdvertiserMercial[] = [];

  public viewsSecData:Array<any> = [
    {data: [0]},
  ];
  public viewsAdsData:Array<any> = [
    {data: [0]}
  ];
  public lineChartLabels:Array<any> = [];
  public lineChartOptions:any = {
    responsive: true,
    scales: {
        yAxes: [{
            display: true,
            ticks: {
                suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                beginAtZero: true   // minimum value will be 0.
            }
        }]
    }
  };
  public viewsSecColors:Array<any> = [];
  public viewsAdsColors:Array<any> = [];
  public lineChartType:string = 'line';
  // Pie
  public viewsTagsLabels:string[] = [];
  public viewsTagsData:number[] = [];
  public viewsTagsColors: Array < any > = [{
   backgroundColor: [],
   borderColor: []
  }];

  public pieChartType:string = 'pie';

  constructor(private api : APIService, private authService : AuthService, private loaderService : LoaderService, private translate : TranslateService) {}

  ionViewWillEnter() {
    this.translate.get("months").subscribe(res => {
      this.lineChartLabels = res
    });
    this.ready = false;
    this.api.analyticsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.posts = res._p;
        this.authService.setPosts(this.posts);
        this.balance = res._b;
        this.secondsViewed = res._sv.toFixed(0);
        this.views = res._v;
        this.topMercial = res._tm;
        this.viewsSecData[0].data = res._vs.d;
        this.viewsSecColors = res._vs.c;

        this.viewsAdsData[0].data = res._va.d;
        this.viewsAdsColors = res._va.c;
        this.viewsTagsData = res._vt.d;
        this.viewsTagsLabels = res._vt.l;
        this.viewsTagsColors[0].backgroundColor = res._vt.colors.fill;
        this.viewsTagsColors[0].borderColor = res._vt.colors.borders;
        this.ready = true;
        this.loaderService.handleLoader(false);
      }
    )
  }

  onRefresh($event) {
    this.loaderService.handleLoader(true);
    this.ready = false;
    this.api.analyticsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.posts = res._p;
        this.authService.setPosts(this.posts);
        this.balance = res._b;
        this.secondsViewed = res._sv.toFixed(0);
        this.views = res._v;
        this.viewsSecData[0].data = res._vs.d;
        this.viewsSecColors = res._vs.c;

        this.viewsAdsData[0].data = res._va.d;
        this.viewsAdsColors = res._va.c;
        this.viewsTagsData = res._vt.d;
        this.viewsTagsLabels = res._vt.l;
        this.viewsTagsColors[0].backgroundColor = res._vt.colors.fill;
        this.viewsTagsColors[0].borderColor = res._vt.colors.borders;

        this.ready = true;
        this.loaderService.handleLoader(false);
        $event.complete();
      }, $event.complete()
    )
  }
}
