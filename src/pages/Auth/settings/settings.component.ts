import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { LoaderService } from "../../../app/loader/loader.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { APIService } from "../../../app/auth/api.service";
import { HelpComponent } from "../help/help.component";
import { AuthService } from "../../../app/auth/auth.service";
import { NoAuthComponent } from "../../NoAuth/noauth.component";
import { App, ModalController, Select } from "ionic-angular";
import { CardIO, CardIOResponse } from '@ionic-native/card-io';
import { StripeCardTokenParams, Stripe } from "@ionic-native/stripe";
import { LegalViewerComponent } from "../../Legal/legal.viewer.component";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-settings",
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
  @ViewChild('legalSelect') legalSelect: Select;
  passwordDisplay : string = "password";
  deletionIsValid : boolean = false;
  accountEmail : string;
  ccvalid : boolean = false;
  ccend : string;
  today : string = new Date().toISOString();
  expDate : string;
  card : StripeCardTokenParams = {
    name : '',
    number : '',
    expMonth : 0,
    expYear : 0,
    cvc : '',
    currency: 'usd'
  };
  paymentFailed : boolean;
  legalDocs = [
    {value : 'advGuide', formatted: "Guidelines"},
    {value : 'privacy-policy', formatted: "Privacy Policy"},
    {value : 'terms-of-use', formatted: "Terms of Service"},
    {value: 'dmca', formatted: "DMCA"},
    {value : 'disclaimer', formatted: "Disclaimer"}
  ];
  accountForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    email : new FormControl('', { validators: [Validators.email, Validators.required], updateOn: 'blur' })
  });
  infoForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    name : new FormControl('', { validators: [Validators.required] })
  });
  paymentForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    "cc-name" : new FormControl('', [Validators.required]),
    "token" : new FormControl(null)
  });
  deleteAccountForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    email : new FormControl({value : '', disabled : false}, [Validators.email, Validators.required]),
  });

  constructor(private api : APIService, private translate: TranslateService, private app: App, private authService : AuthService, private loaderService : LoaderService, private snackbarService : SnackbarService, private cardIO: CardIO, private stripe: Stripe) {
    this.stripe.setPublishableKey(process.env.STRIPE_PUBLIC);
  }

  ngOnInit() {
    this.translate.get("legal$list").subscribe((res : string[]) => {
      for (let i = 0; i < res.length; i++) {
        this.legalDocs[i].formatted = res[i];
      }
    });
    this.api.settingsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.accountForm.controls.email.setValue(res._a.email);
        this.infoForm.controls.name.setValue(res._i.name);
        this.ccvalid = res._p.valid;
        this.ccend = res._p.end;
        this.paymentFailed = res._p.failed;
        this.accountEmail = res._a.email;
        this.loaderService.handleLoader(false);
      }
    )
  }

  togglePassword(hide : boolean) {
    this.passwordDisplay = hide ? 'password' : 'text';
  }

  validateDeletion() {
    const valid = this.deleteAccountForm.value.email.toLowerCase() === this.accountEmail;
    this.deletionIsValid = valid;
  }

  numChanged($event) {
    if (!$event) {
      return;
    }
    let formattedNum = $event.split('-').join('');
    if (formattedNum.length > 0) {
      formattedNum = formattedNum.match(new RegExp('.{1,4}', 'g')).join('-');
    }
    this.card.number = formattedNum;
  }

  onAccountSubmit() {
    if (this.accountForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updateAccount(this.accountForm.value, this.loaderService).subscribe(
      (response : any) => {
        const res = response._body;

        if (res._m === "_emailinuse") {
          this.snackbarService.handleError({"message" : "email_inuse", "isError" : true});
          this.accountForm.controls.email.setErrors({'inuse':true});
        } else {
          this.snackbarService.handleError({"message" : "account_updated", "isError" : false})
          this.accountForm.reset();
          this.accountForm.controls.auth.setValue(this.authService.getAUTH());
        }
        this.loaderService.handleLoader(false);
      }
    );
  }

  onInfoSubmit() {
    if (this.infoForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updateInfo(this.infoForm.value, this.loaderService).subscribe(
      (response : any) => {
        const res = response._body;
        this.snackbarService.handleError({"message" : "info_updated", "isError" : false})
        this.infoForm.reset(this.infoForm.value);
        this.loaderService.handleLoader(false);
      }
    );
  }

  onPaymentSubmit() {
    if (this.paymentForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);

    if (this.paymentForm.value["cc-name"].length === 0) {
      this.paymentForm.controls.token.setValue("_emptytkn");
      this.api.updatePayment(this.paymentForm.value, this.loaderService).subscribe(
        (response : any) => {
          const res = response._body;
          this.snackbarService.handleError({"message" : "cc_removed", "isError" : false});
          this.ccvalid = false;
          this.ccend = null;
          this.paymentForm.reset({
            'auth' : this.authService.getAUTH()
          });
          this.card = {
            name : '',
            number : '',
            expMonth : 0,
            expYear : 0,
            cvc : '',
            currency: 'usd'
          };
          this.loaderService.handleLoader(false);
        }
      );
      return;
    }

    const name = this.paymentForm.get('cc-name').value;
    try {
      this.card.name = name;
      this.card.number = this.card.number.replace(/[^0-9]/g, "");
      this.card.expMonth = parseInt(this.expDate.split("-")[1]);
      this.card.expYear = parseInt(this.expDate.split("-")[0]);
    } catch (Error) {
      this.snackbarService.handleError({"message" : "invalid_card_details", "isError" : true});
      this.loaderService.handleLoader(false);
      return;
    }

    this.stripe.validateCardNumber(this.card.number).then(()=>this.stripe.validateExpiryDate(this.expDate.split("-")[1], this.expDate.split("-")[0])).then(()=>this.stripe.validateCVC(this.card.cvc)).then(()=>{
      this.stripe.createCardToken(this.card).then(token => {
       if (!token) {
         this.loaderService.handleLoader(false);
         return;
       }
       this.paymentForm.controls.token.setValue(token);
       this.api.updatePayment(this.paymentForm.value, this.loaderService).subscribe(
         (response : any) => {
           const res = response._body;
           if (res._m == "_stripeerror") {
             this.snackbarService.handleError({"message" : "stripe_val_error", "isError" : true});
             this.loaderService.handleLoader(false);
             return;
           }
           this.snackbarService.handleError({"message" : "cc_updated", "isError" : false});
           this.ccvalid = true;
           this.card = {
             name : '',
             number : '',
             expMonth : 0,
             expYear : 0,
             cvc : '',
             currency: 'usd'
           };
           this.ccend = token.card.last4;
           this.paymentForm.reset({
             'auth' : this.authService.getAUTH()
           });
           this.loaderService.handleLoader(false);
         });
      }).catch(error => {
        this.loaderService.handleLoader(false);
        this.snackbarService.handleError({"message" : "invalid_card_details", "isError" : true});
     });
   }).catch(()=>{
      this.snackbarService.handleError({"message" : "invalid_card_details", "isError" : true});
      this.loaderService.handleLoader(false);
    })
  }

  onScanCard() {
    this.cardIO.canScan().then(
      (res: boolean) => {
        if(res){
          let options = {
            requireCardholderName: true,
            requireExpiry: true,
            requireCVV: true,
            useCardIOLogo: true,
            scanExpiry: true,
            keepApplicationTheme: true,
            requirePostalCode: false,
            guideColor: "#663399"
          };
          this.cardIO.scan(options).then((response : CardIOResponse) => {
            this.paymentForm.controls["cc-name"].setValue(response.cardholderName);
            this.numChanged(response.cardNumber);
            let d = new Date();
            d.setMonth(response.expiryMonth - 1);
            d.setFullYear(response.expiryYear);
            this.expDate = d.toISOString();
            this.card.cvc = response.cvv;
          }).catch(error => {
          });
        }
      }
    ).catch(error => {
    });
  }

  onLegalView($event) {
    if ($event.length === 0) return;
    this.app.getRootNav().push(LegalViewerComponent, {type: $event});
    this.legalSelect.setValue(null);
  }

  onDeleteSubmit() {
    if (this.deleteAccountForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.deleteAccount(this.deleteAccountForm.value, this.loaderService).subscribe(
      (response : any) => {
        this.snackbarService.handleError({"message" : "account_deleted", "isError" : false})
        this.deleteAccountForm.reset(this.deleteAccountForm.value);
        this.loaderService.handleLoader(false);
        this.authService.logout();
      }
    );
  }
}
