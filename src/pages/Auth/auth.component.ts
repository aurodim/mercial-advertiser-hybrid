import { Component, ViewChild } from "@angular/core";
import { NavController, App, Platform, ModalController, FabContainer, Tabs, NavParams } from "ionic-angular";
import { AuthService } from "../../app/auth/auth.service";
import { HelpComponent } from "./help/help.component";
import { SettingsComponent } from "./settings/settings.component";
import { NoAuthComponent } from "../NoAuth/noauth.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { MercialsComponent } from "./mercials/mercials.component";
import { Subscription } from "rxjs";
import { HelperViewerService } from "./help/viewer/viewer.helper.service";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

@Component({
  selector: 'app-auth',
  templateUrl: 'auth.component.html'
})
export class AuthComponent {
  @ViewChild('tabsRef') tabRef: Tabs;
  navController : NavController;
  rootPage:any = DashboardComponent;
  Dashboard = DashboardComponent;
  Help = HelpComponent;
  Mercials = MercialsComponent;
  Settings = SettingsComponent;
  tabsOrder = [0,1,2];
  showHelper = false;
  helperURL : string = "";
  loaded:   boolean = false;
  tabIndex: number  = 0;
  subscription: Subscription;
  logOutEmition: Subscription;

  constructor(private app : App, private navParams : NavParams, private modalCtrl : ModalController, private platform : Platform, private authService : AuthService, private helperViewerService : HelperViewerService, private nativePageTransitions: NativePageTransitions) {
    this.subscription = this.helperViewerService.getTrigger().subscribe((data) => { this.showHelper = data.show; this.helperURL = data.url });
    this.logOutEmition = this.authService.getSubcription().subscribe((logout) => {
      if (!logout) return;
      this.navController.push(NoAuthComponent).then(()=>{
        this.navController.remove(0,1);
      });
    });
  }

  ionViewDidLoad() {
    this.navController = this.app.getRootNav();
    var firstTime : boolean = this.navParams.get("firstTime");
    if (firstTime) {
      this.helperURL = "https://res.cloudinary.com/aurodim/video/upload/v1546473150/mercial/static/explainers/business-advertiser-beginner.mp4";
      this.showHelper = true;
    }
  }

  OnInit() {
    this.navController = this.app.getRootNav();
  }

  onSwipe($event) {
    if ($event.overallVelocityX > 0) {
      let ind = this.tabRef.getSelected().index - 1;
      if (ind == -1) return;
      this.tabRef.select(ind);
    } else {
      let ind = this.tabRef.getSelected().index + 1;
      if (ind == 4) return;
      this.tabRef.select(ind);
    }
  }

  onTabChange($event) {
    if ($event.index == 0 || $event.index == 4) return;

    this.tabsOrder = [$event.index - 1, $event.index, $event.index + 1];

    if (!this.loaded) {
      this.loaded = true;
      return;
    }
  }

  ionViewCanEnter() {
      this.authService.isAuthenticated();
  }

  ionViewCanLeave() {
      !this.authService.isAuthenticated();
  }

  ngOnDestroy() {
    this.logOutEmition.unsubscribe();
    this.subscription.unsubscribe();
  }

  onLogout() {
    this.authService.logout();
  }
}
