import { Component, ViewChild, OnInit } from "@angular/core";
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { AdvertiserMercialsService } from "../mercials.service";
import { AuthService } from "../../../../app/auth/auth.service";
import { APIService } from "../../../../app/auth/api.service";
import { LoaderService } from "../../../../app/loader/loader.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { NavParams, NavController } from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-edit-mercial',
  templateUrl: 'edit-mercial.component.html'
})
export class MercialsEditComponent {
  id : string;
  sizes = [
      { value : "small", init : true, formatted : "Small - $0.002" },
      { value : "medium", init : false, formatted : "Medium - $0.003" },
      { value : "large", init : false, formatted : "Large - $0.004" },
  ];
  pageNumber : number = 1;

  editForm = new FormGroup({
    auth : new FormControl(this.api.AUTH(), [Validators.required]),
    id : new FormControl(this.id, [Validators.required]),
    title : new FormControl("", [Validators.required]),
    description : new FormControl(""),
    size : new FormControl("", [Validators.required]),
    tags : new FormControl("", [Validators.required]),
    more : new FormControl(""),
    moreSecond : new FormControl(""),
    moreThird : new FormControl(""),
    moreFourth : new FormControl(""),
    moreFifth : new FormControl(""),
    moreSixth : new FormControl(""),
  });

  constructor(private authService : AuthService, private navParams : NavParams, private navController : NavController, private api : APIService, private mercialsService : AdvertiserMercialsService, private loaderService : LoaderService, private snackbarService : SnackbarService, private translate : TranslateService) {}

  ionViewWillEnter() {
    this.translate.get("sizes$list").subscribe((res : string[]) => {
      for (let i = 0; i < res.length; i++) {
        this.sizes[i].formatted = res[i];
      }
    });
    this.id = this.navParams.get("id");
    this.api.editorData(this.id).subscribe(
      (response : any) => {
        const res = response._body._d;

        if (!res._m) {
          this.snackbarService.handleError({"message" : "mercial_not_found", "isError" : true})
          this.loaderService.handleLoader(false);
          this.navController.pop();
          return;
        }

        this.editForm.controls["id"].setValue(res._m.id);
        this.editForm.controls.title.setValue(res._m.title);
        this.editForm.controls.description.setValue(res._m.description);
        this.editForm.controls.size.setValue(res._m.size);
        this.editForm.controls.tags.setValue(res._m.tags.join(','));
        if (res._m.more) {
          for(let i = 0; i < res._m.more.length; i++) {
            switch(i) {
              case 0:
                this.editForm.controls.more.setValue(res._m.more[i]);
                break;
              case 1:
                this.editForm.controls.moreSecond.setValue(res._m.more[i]);
                break;
              case 2:
                this.editForm.controls.moreThird.setValue(res._m.more[i]);
                break;
              case 3:
                this.editForm.controls.moreFourth.setValue(res._m.more[i]);
                break;
              case 4:
                this.editForm.controls.moreFifth.setValue(res._m.more[i]);
                break;
              case 5:
                this.editForm.controls.moreSixth.setValue(res._m.more[i]);
                break;
            }
          }
        }
        this.loaderService.handleLoader(false);
      }
    );
  }

  onDelete() {
    let obj = {
      auth : this.authService.getAUTH(),
      id : this.id,
      d : new Date().getTime()
    };
    this.loaderService.handleLoader(true);
    this.api.deleteMercial(obj, this.loaderService).subscribe(
      (response : any) => {
        const res = response._body;

        this.snackbarService.handleError({"message" : "mercial_deleted", "isError" : false});
        this.mercialsService.remove(this.id);
        this.loaderService.handleLoader(false);
        this.navController.pop();
      }
    );
  }

  onSubmit() {
    if (this.editForm.invalid) {
      return;
    }

    this.loaderService.handleLoader(true);
    this.api.updateMercial(this.editForm.value, this.loaderService).subscribe(
      (response : any) => {
        const res = response._body;

        this.snackbarService.handleError({"message" : "mercial_updated", "isError" : false})
        this.editForm.reset(this.editForm.value);
        this.loaderService.handleLoader(false);
        this.mercialsService.reloadMercials();
        this.navController.pop();
      }
    );
  }

  onBack() {
    this.pageNumber -= 1;
  }

  onNext() {
    this.pageNumber += 1;
  }
}
