import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { AdvertiserMercial } from "./mercial/advertiser-mercial.model";
import { APIService } from "../../../app/auth/api.service";
import { LoaderService } from "../../../app/loader/loader.service";

@Injectable()
export class AdvertiserMercialsService {
  mercialsChanged = new Subject<AdvertiserMercial[]>();

  private mercials : AdvertiserMercial[] = [];

  constructor(private api : APIService, private loaderService : LoaderService) {}

  requestMercials(callback : Function) {
    this.api.mercialsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.mercials = [];
        for (let mercial of res._c) {
          this.mercials.push(new AdvertiserMercial(mercial.id, mercial.title, mercial.thumbnail, mercial.size, mercial.tags, mercial.duration, mercial.paused, mercial.description));
        }
        this.loaderService.handleLoader(false);
        callback();
      }
    );
  }

  reloadMercials() {
    this.api.mercialsData().subscribe(
      (response : any) => {
        this.mercials = [];
        const res = response._body._d;
        for (let mercial of res._c) {
          this.mercials.push(new AdvertiserMercial(mercial.id, mercial.title, mercial.thumbnail, mercial.size, mercial.tags, mercial.duration, mercial.paused, mercial.description))
        }
        this.mercialsChanged.next(this.mercials.slice());
        this.loaderService.handleLoader(false);
      }
    );
  }

  getMercial(id:string) {
    return this.mercials.find(mercial => mercial.id === id);
  }

  getMercials() {
    return this.mercials.slice();
  }

  add(mercial : AdvertiserMercial) {
    this.mercials.push(new AdvertiserMercial(mercial.id, mercial.title, mercial.thumbnail, mercial.size, mercial.tags, mercial.duration, mercial.paused, mercial.description))
    this.mercialsChanged.next(this.mercials.slice());
  }

  remove(id : string) {
    this.mercials = this.mercials.filter((mercial) => mercial.id != id);
    this.mercialsChanged.next(this.mercials.slice());
  }
}
