import { Component, HostListener } from "@angular/core";
import { Subscription } from "rxjs";
import { AdvertiserMercial } from "./mercial/advertiser-mercial.model";
import { AdvertiserMercialsService } from "./mercials.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { ModalController } from "ionic-angular";
import { MercialsNewComponent } from "./new/new-mercial.component";

@Component({
  selector: 'app-advertiser-mercials',
  templateUrl: 'mercials.component.html'
})
export class MercialsComponent {
  subscription : Subscription;
  mercials: AdvertiserMercial[] = [];

  constructor(private mercialsService : AdvertiserMercialsService, private loaderService : LoaderService, private modalCtrl: ModalController) {}

  ionViewWillEnter() {
    this.mercialsService.requestMercials(() => {
      this.mercials = this.mercialsService.getMercials();
    });
    this.subscription = this.mercialsService.mercialsChanged.subscribe(
      (mercials : AdvertiserMercial[]) => {
        this.mercials = mercials;
      }
    );
  }

  onSearch($event) {
    this.mercials = this.mercialsService.getMercials().filter((mercial) => mercial.title.toLowerCase().includes($event.target.value.toLowerCase()) || mercial.description.toLowerCase().includes($event.target.value.toLowerCase()));
  }

  onAddClick() {
    let newModal = this.modalCtrl.create(MercialsNewComponent);
    newModal.present();
  }

  onRefresh($event) {
    this.loaderService.handleLoader(true);
    this.mercialsService.requestMercials(() => {
      this.mercials = this.mercialsService.getMercials();
      $event.complete();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
