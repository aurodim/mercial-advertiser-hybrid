export class AdvertiserMercial {
  id : string;
  title : string;
  thumbnail : string;
  size : string;
  description? : string;
  tags : string[];
  paused : boolean;
  duration : number;
  widthPixel : number;
  heightPixel : number;

  constructor(id : string, title : string, thumbnail : string, size : string, tags : string[], duration : number, paused : boolean, description? : string) {
    if (size == "large") {this.widthPixel = 450; this.heightPixel = 350;}
    else if (size == "medium") {this.widthPixel = 350; this.heightPixel = 300;}
    else {this.widthPixel = 250; this.heightPixel = 250;}

    this.id = id;
    this.title = title;
    this.thumbnail = thumbnail;
    this.size = size;
    this.tags = tags;
    this.paused = paused;
    this.duration = duration;
    this.description = description;
  }

  public getTags() {
    return this.tags.slice().join(" / ");
  }

  public getSigns() {
    if (this.size == "small") return "$";
    else if (this.size == "medium") return "$$";
    else return "$$$";
  }

  public Cloudinarize(link : string, width : number = window.innerWidth, extras? : string) {
    return link.replace("/upload/", `/upload/w_${width}${extras ? ','+extras : ''}/`);
  }
}
