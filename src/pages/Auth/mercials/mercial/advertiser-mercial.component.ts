import { Component, Input, OnInit } from "@angular/core";
import { AdvertiserMercial } from "./advertiser-mercial.model";
import { MercialsEditComponent } from "../edit/edit-mercial.component";
import { ModalController, AlertController } from "ionic-angular";
import { MercialsAnalyticsComponent } from "../analytics/analytics-mercial.component";
import { AuthService } from "../../../../app/auth/auth.service";
import { APIService } from "../../../../app/auth/api.service";
import { AdvertiserMercialsService } from "../mercials.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-mercial',
  templateUrl: 'advertiser-mercial.component.html'
})
export class MercialComponent implements OnInit {
  @Input("mercialVideo") mercial : AdvertiserMercial;
  loadingPause = false;
  translationStrings = [];

  constructor(public modalCtrl: ModalController, private alertCtrl : AlertController, private authService : AuthService, private api : APIService, private mercialsService : AdvertiserMercialsService, private translate : TranslateService) {

  }

  ngOnInit() {
    this.translate.get("tags").subscribe(res => this.translationStrings.push(res));
    this.translate.get("ok").subscribe(res => this.translationStrings.push(res));
  }

  onPaused() {
    this.mercial.paused = !this.mercial.paused;
    this.loadingPause = true;
    let obj = {
      auth : this.authService.getAUTH(),
      id : this.mercial.id,
      d : new Date().getTime()
    };
    this.api.pauseMercial(obj, () => {this.loadingPause = false;this.mercial.paused = !this.mercial.paused}).subscribe(
      (response : any) => {
        const res = response._body;

        if (res._m === "_notfound")  {
          this.mercialsService.remove(this.mercial.id);
        }
        this.loadingPause = false;
        this.mercial.paused = res._d._p;
      }
    );
  }

  onView() {
    let editModal = this.modalCtrl.create(MercialsEditComponent, { id: this.mercial.id });
    editModal.present();
  }

  onAnalytics() {
    let analyticsModal = this.modalCtrl.create(MercialsAnalyticsComponent, { id: this.mercial.id });
    analyticsModal.present();
  }

  onTagsClick() {
    let alert = this.alertCtrl.create({
      title: this.translationStrings[0],
      message: this.mercial.tags.slice().join("<br/><br/>"),
      buttons: [this.translationStrings[1]]
    });
    alert.present();
  }
}
