import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { AdvertiserMercialsService } from "../mercials.service";
import { AuthService } from "../../../../app/auth/auth.service";
import { APIService } from "../../../../app/auth/api.service";
import { LoaderService } from "../../../../app/loader/loader.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { NavController } from "ionic-angular";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-new-mercial',
  templateUrl: 'new-mercial.component.html'
})
export class MercialsNewComponent implements OnInit {
  sizes = [
      { value : "small", init : true, formatted : "Small - $0.002" },
      { value : "medium", init : false, formatted : "Medium - $0.003" },
      { value : "large", init : false, formatted : "Large - $0.0004" },
  ];
  ethnicities = [
    { value : "asian/pacisl", init : false, formatted : "Asian / Pacific Islander" },
    { value : "black/afriame", init : false, formatted : "Black or African American" },
    { value : "hispanic/latino", init : false, formatted : "Hispanic or Latino" },
    { value : "natamer/ameind", init : false, formatted : "Native American or American Indian" },
    { value : "other", init : false, formatted : "Other" },
    { value : "white", init : true, formatted : "White" }
  ];
  locations = [
      { value : "country", init : false, formatted : "Country" },
      { value : "state", init : false, formatted : "State" },
      { value : "zipcode", init : false, formatted : "Zipcode / Postal Code" },
      { value : "city", init : false, formatted : "City" },
  ];
  languages = [
    { value : "en", formatted : "English"},
    { value : "es", formatted : "Español"}
  ];
  fileURI:any;
  fileName:string;
  pageNumber : number = 1;
  newMercialForm = new FormGroup({
    auth : new FormControl(this.api.AUTH(), [Validators.required]),
    title : new FormControl("", [Validators.required]),
    description : new FormControl(""),
    more : new FormControl("", [Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/)]),
    moreSecond : new FormControl("", [Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/)]),
    moreThird : new FormControl("", [Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/)]),
    moreFourth : new FormControl("", [Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/)]),
    moreFifth : new FormControl("", [Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/)]),
    moreSixth : new FormControl("", [Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/)]),
    size : new FormControl("", [Validators.required]),
    tags : new FormControl("", [Validators.required]),
    age : new FormControl("13", [Validators.required]),
    ethnicities : new FormControl([]),
    languages : new FormControl([]),
    locationType : new FormControl(""),
    locations : new FormControl("")
  });

  constructor(private authService : AuthService, private api : APIService, private navController : NavController, private mercialsService : AdvertiserMercialsService, private loaderService : LoaderService, private snackbarService : SnackbarService, private transfer: FileTransfer, private camera: Camera, private translate : TranslateService) {}

  ngOnInit() {
    this.translate.get("sizes$list").subscribe((res : string[]) => {
      for (let i = 0; i < res.length; i++) {
        this.sizes[i].formatted = res[i];
      }
    });
    this.translate.get("ethnicities$list").subscribe((res : string[]) => {
      for (let i = 0; i < res.length; i++) {
        this.ethnicities[i].formatted = res[i];
      }
    });
    this.translate.get("locations$list").subscribe((res : string[]) => {
      for (let i = 0; i < res.length; i++) {
        this.locations[i].formatted = res[i];
      }
    });
  }

  onSubmit() {
    if (this.newMercialForm.invalid || !this.fileURI) {
      return;
    }
    if (!/^(1[3-9]|[2-9][0-9])(-1[3-9]|-[2-9][0-9])?$/.test(this.newMercialForm.value.age)) return;
    if (this.newMercialForm.value.more.length > 0 && !/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/.test(this.newMercialForm.value.more)) return;
    if (this.newMercialForm.value.moreSecond.length > 0 && !/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/.test(this.newMercialForm.value.moreSecond)) return;
    if (this.newMercialForm.value.moreThird.length > 0 && !/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/.test(this.newMercialForm.value.moreThird)) return;
    if (this.newMercialForm.value.moreFourth.length > 0 && !/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/.test(this.newMercialForm.value.moreFourth)) return;
    if (this.newMercialForm.value.moreFifth.length > 0 && !/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/.test(this.newMercialForm.value.moreFifth)) return;
    if (this.newMercialForm.value.moreSixth.length > 0 && !/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\https?:\/\/.+'\(\)\*\+,;=.]+$/.test(this.newMercialForm.value.moreSixth)) return;

    this.loaderService.handleLoader(true);
    const fileTransfer: FileTransferObject = this.transfer.create();
    let fName = this.fileName;
    let options: FileUploadOptions = {
      fileKey: 'video',
      fileName: 'mercial-file-'+new Date().getTime()+'.'+fName.split('.')[1],
      chunkedMode: false,
      mimeType: "video/"+fName.split('.')[1],
      headers: {"Accept" : 'application/json'},
      params: {
        'auth' : this.authService.getAUTH(),
        'description' : this.newMercialForm.value.description,
        'title' : this.newMercialForm.value.title,
        'tags' : this.newMercialForm.value.tags,
        'size' : this.newMercialForm.value.size,
        'age' : this.newMercialForm.value.age,
        'more' : this.newMercialForm.value.more,
        'moreSecond' : this.newMercialForm.value.moreSecond,
        'moreThird' : this.newMercialForm.value.moreThird,
        'moreFourth' : this.newMercialForm.value.moreFourth,
        'moreFifth' : this.newMercialForm.value.moreFifth,
        'moreSixth' : this.newMercialForm.value.moreSixth,
        'ethnicities' : this.newMercialForm.value.ethnicities.join(),
        'languages' : this.newMercialForm.value.languages.join(''),
        'locationType' : this.newMercialForm.value.locationType,
        'locations' : this.newMercialForm.value.locations,
      }
    };

    fileTransfer.upload(this.fileURI, this.api.api + '/api/advertiser/mercials/new', options)
      .then((response : any) => {

        const res = JSON.parse(response.response)._body;

        try {

          if (res._m == "_invalidpayment") {
            this.snackbarService.handleError({"message" : "invalid_payment", "isError" : false});
            this.loaderService.handleLoader(false);
            return;
          }

          if (res._m == "_invalidsize") {
            this.snackbarService.handleError({"message" : "invalid_size", "isError" : false});
            this.loaderService.handleLoader(false);
            return;
          }

          if (res._m == "_invalidage") {
            this.snackbarService.handleError({"message" : "invalid_age", "isError" : false});
            this.loaderService.handleLoader(false);
            return;
          }
        } catch (Error) {
        }

      this.mercialsService.add(res._c);
      this.snackbarService.handleError({"message" : "mercial_uploaded", "isError" : false});
      this.newMercialForm.reset();
      this.loaderService.handleLoader(false);
      this.navController.pop();
    }, (err) => {
      this.loaderService.handleLoader(false);
      this.snackbarService.handleError({message:'mercial_upload_error', isError:true});
    }).catch(err => {
      this.loaderService.handleLoader(false);
      this.snackbarService.handleError({message:'mercial_upload_error', isError:true});
    });
  }

  onChooseVideo() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.VIDEO
    }

    this.camera.getPicture(options).then((imageData) => {
      this.fileURI = imageData;
      this.fileName = imageData.toString();
    }, (err) => {
      if (err === "has no access to assets") {
        return;
      }
      this.snackbarService.handleError({message : "video_select_error", isError : true})
    });
  }

  onBack() {
    this.pageNumber -= 1;
  }

  onNext() {
    this.pageNumber += 1;
  }
}
