import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";

@Injectable()
export class HelperViewerService {
  private subject = new Subject<any>();

  triggerVideo(show : boolean, url : string = "") {
    this.subject.next({show : show, url : url});
  }

  getTrigger() : Observable<any> {
    return this.subject.asObservable();
  }
}
