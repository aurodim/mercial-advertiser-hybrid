export class FAQ {
  param : string;
  title : string;
  answer : string;

  constructor(param : string, title : string, answer : string) {
    this.param = param;
    this.title = title;
    this.answer = answer;
  }
}
