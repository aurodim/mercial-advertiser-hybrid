import { Component, OnInit } from "@angular/core";
import { FAQ } from "./faq.model";
import { APIService } from "../../../app/auth/api.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { NavParams, ModalController } from "ionic-angular";
import { MessengerComponent } from "./messenger/messenger.component";

@Component({
  selector: "app-help",
  templateUrl: "./help.component.html"
})
export class HelpComponent implements OnInit {
  fragment : string;
  faqs : FAQ[] = [];
  faqsBackup : FAQ[] = [];

  constructor(private navParams: NavParams, private modalController : ModalController, private api : APIService, private loaderService : LoaderService) {}

  ngOnInit() {
    this.api.helpData().subscribe(
      (response : any) => {
        this.faqs = [];
        this.faqsBackup = [];
        const res = response._body._d;
        for (let faq of res._f) {
          this.faqsBackup.push(new FAQ(faq.param, faq.title, faq.answer))
          this.faqs.push(new FAQ(faq.param, faq.title, faq.answer))
        }
        if (this.fragment) {
          setTimeout(() => {
            this.scrollToFragment();
          }, 1000);
        }
        this.loaderService.handleLoader(false);
      }
    );
    this.fragment = this.navParams.get("fragment");
    this.scrollToFragment();
  }

  onSearch($event) {
    this.faqs = this.faqsBackup.filter((faq) => faq.title.toLowerCase().includes($event.target.value.toLowerCase()) || faq.answer.toLowerCase().includes($event.target.value.toLowerCase()));
  }

  onMessageOpen() {
    let messageModal = this.modalController.create(MessengerComponent);
    messageModal.present();
  }

  scrollToFragment() {
    const element = document.getElementById(this.fragment);
    if (!element) {
      return;
    }
    element.scrollIntoView(true);
  }
}
