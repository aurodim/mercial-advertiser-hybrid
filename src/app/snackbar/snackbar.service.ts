import { ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SnackbarService {
  private positiveTitles : string[] = ["awesome", "great", "yay", "woohoo", "success"];
  private negativeTitles : string[] = ["error", "sorry", "this_sucks", "unexpected", "oh_no", "oh_oh"];

  constructor(private toastCtrl: ToastController, private translator : TranslateService) {}

  public handleError(data : any) {
    let title;

    title = data.isError ? "snackbar.negative."+this.negativeTitles[Math.floor(Math.random() * this.negativeTitles.length)] : "snackbar.positive."+this.positiveTitles[Math.floor(Math.random() * this.positiveTitles.length)];

    if (data.raw) {
      data.message = data.message;
    } else {
      data.message = 'snackbar.'+data.message;
    }

    this.translator.get(title).subscribe(titleTranslated=>{
      this.translator.get(data.message).subscribe(messageTranslated=>{
        let toast = this.toastCtrl.create({
          message: `${titleTranslated} ${data.raw ? data.message : messageTranslated}`,
          duration: 3000,
          position: 'bottom',
          showCloseButton : true
        });


        toast.present();
      });
    });
  }
}
