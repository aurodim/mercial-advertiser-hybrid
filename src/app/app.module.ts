import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts';

import { MercialAdvertisers } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LoaderModule } from './loader/loader.module';
import { Keyboard } from '@ionic-native/keyboard';
import { APIService } from './auth/api.service';
import { AuthService } from './auth/auth.service';
import { SnackbarService } from './snackbar/snackbar.service';
import { LoaderService } from './loader/loader.service';
import { NoAuthComponent } from '../pages/NoAuth/noauth.component';
import { AuthComponent } from '../pages/Auth/auth.component';
import { LandingComponent } from '../pages/NoAuth/landing/landing.component';
import { LoginComponent } from '../pages/NoAuth/login/login.component';
import { RegisterComponent } from '../pages/NoAuth/register/register.component';
import { DashboardComponent } from '../pages/Auth/dashboard/dashboard.component';
import { HelpComponent } from '../pages/Auth/help/help.component';
import { SettingsComponent } from '../pages/Auth/settings/settings.component';
import { MercialsComponent } from '../pages/Auth/mercials/mercials.component';
import { MercialsNewComponent } from '../pages/Auth/mercials/new/new-mercial.component';
import { MercialComponent } from '../pages/Auth/mercials/mercial/advertiser-mercial.component';
import { MercialsEditComponent } from '../pages/Auth/mercials/edit/edit-mercial.component';
import { AdvertiserMercialsService } from '../pages/Auth/mercials/mercials.service';

import { Globalization } from '@ionic-native/globalization';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { CardIO } from '@ionic-native/card-io';
import { Network } from '@ionic-native/network';
import { MessengerComponent } from '../pages/Auth/help/messenger/messenger.component';
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { Stripe } from '@ionic-native/stripe';
import { HelpViewerComponent } from '../pages/Auth/help/viewer/viewer.helper.component';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { HelperViewerService } from '../pages/Auth/help/viewer/viewer.helper.service';
import { MercialsAnalyticsComponent } from '../pages/Auth/mercials/analytics/analytics-mercial.component';
import { LegalViewerComponent } from '../pages/Legal/legal.viewer.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, `https://res.cloudinary.com/aurodim/raw/upload/v${process.env.VERSION}/mercial/static/translations/`, ".json");
}
const GoogleMapsCore = AgmCoreModule.forRoot({
  apiKey : 'AIzaSyCN7Lr-FZ2irzlktTPr23ewRK1fSHcWQ9s',
  libraries: ['places']
});

@NgModule({
  declarations: [
    MercialAdvertisers,
    NoAuthComponent,
    AuthComponent,
    LegalViewerComponent,
    LandingComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    HelpViewerComponent,
    HelpComponent,
    MessengerComponent,
    MercialsComponent,
    MercialsNewComponent,
    MercialComponent,
    MercialsAnalyticsComponent,
    MercialsEditComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    TranslateModule.forRoot({
       loader: {
           provide: TranslateLoader,
           useFactory: HttpLoaderFactory,
           deps: [HttpClient]
       }
    }),
    IonicModule.forRoot(MercialAdvertisers,
      {
        autocomplete: 'on',
        platforms : {
          ios : {
            // These options are available in ionic-angular@2.0.0-beta.2 and up.
            scrollAssist: false,    // Valid options appear to be [true, false]
            autoFocusAssist: 'false'  // Valid options appear to be ['instant', 'delay', false]
          },
          android : {
            // These options are available in ionic-angular@2.0.0-beta.2 and up.
            scrollAssist: false,    // Valid options appear to be [true, false]
            autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
          }
          // http://ionicframework.com/docs/v2/api/config/Config/)
        }
      }
    ),
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'aurodim' } as CloudinaryConfiguration),
    IonicStorageModule.forRoot({
      name: '__meradveesdb',
    }),
    HttpClientModule,
    LoaderModule,
    ChartsModule,
    GoogleMapsCore
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MercialAdvertisers,
    AuthComponent,
    NoAuthComponent,
    LegalViewerComponent,
    LandingComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    HelpViewerComponent,
    HelpComponent,
    MessengerComponent,
    MercialsComponent,
    MercialsNewComponent,
    MercialComponent,
    MercialsAnalyticsComponent,
    MercialsEditComponent,
    SettingsComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    APIService,
    AuthService,
    SnackbarService,
    AdvertiserMercialsService,
    LoaderService,
    File,
    Globalization,
    FileTransfer,
    Network,
    Stripe,
    HelperViewerService,
    FileTransferObject,
    Camera,
    CardIO,
    NativePageTransitions,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
