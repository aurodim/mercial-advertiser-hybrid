import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/retry';
import { SnackbarService } from "../snackbar/snackbar.service";
import { LoaderService } from "../loader/loader.service";
import { App } from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";

@Injectable()
export class APIService {
  public api : string;

  constructor(private http : HttpClient, private app : App, private authService : AuthService, private snackbarService : SnackbarService, private loaderService : LoaderService, private translate : TranslateService) {
    this.api = process.env.BASE_URL;
  }

  AUTH() {
    return this.authService.getAUTH();
  }

  // GET
  analyticsData() : Observable<Object> {
    return this.http
    .get(this.api + "/api/advertiser/analytics", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  helpData() : Observable<Object> {
    return this.http
    .get(this.api + "/api/advertiser/help", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  mercialsData() : Observable<Object> {
    return this.http
    .get(this.api + "/api/advertiser/mercials", { params : { 'auth' : this.authService.getAUTH(), 'lang' : this.translate.getDefaultLang()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  mercialAnalyticsData(id : string) : Observable<Object> {
    return this.http
    .get(this.api + "/api/advertiser/mercials/analytics", { params : { 'auth' : this.authService.getAUTH(), 'mercial' : id}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  editorData(id : string) : Observable<Object> {
    return this.http
    .get(this.api + "/api/advertiser/mercials/edit", { params : { 'auth' : this.authService.getAUTH(), 'mercial' : id, 'lang' : this.translate.getDefaultLang()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  settingsData() : Observable<Object> {
    return this.http
    .get(this.api + "/api/advertiser/settings", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  // POST

  register(requirements : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(requirements);
    return this.http.post(this.api + "/api/advertiser/register", body, {"headers" : headers});
  }

  login(credentials : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    return this.http.post(this.api + "/api/advertiser/login", body, {"headers" : headers});
  }

  deleteMercial(values : object, loader : LoaderService) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/advertiser/mercials/delete", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateMercial(values : any, loader : LoaderService) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/advertiser/mercials/edit?id="+values.id, body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  pauseMercial(values : any, callback: Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/advertiser/mercials/pause?id="+values.id, body, { headers : headers })
    .catch((error : HttpErrorResponse) => {
      callback();
      return this.ErrorHandler(error)
    });
  }

  uploadMercial(values : object, loader : LoaderService) : Observable<Object> {
    const headers = new HttpHeaders({"Accept" : 'application/json'});
    const body = values;
    return this.http.post(this.api + "/api/advertiser/mercials/new", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateAccount(values : object, loader : LoaderService) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/advertiser/settings/account", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateInfo(values : object, loader : LoaderService) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/advertiser/settings/info", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updatePayment(values : object, loader : LoaderService) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/advertiser/settings/payment", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  deleteAccount(values : object, loader : LoaderService) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/advertiser/delete", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  ErrorHandler(error : HttpErrorResponse, onError? : Function) {
    this.snackbarService.handleError({"message" : error.status + " " + error.statusText, "isError" : true, raw : true});
    if (error.status === 401) {
      this.authService.logout();
      return Observable.empty<null>();
    }
    if (onError) onError();
    this.loaderService.handleLoader(false);
    return Observable.empty<null>();
  }
}
