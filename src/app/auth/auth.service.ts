import { Injectable, EventEmitter } from "@angular/core";
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthService {
  private loggedIn : boolean = false;
  private _at : string = null;
  private posts : number = 0;
  private subscription : EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private storage: Storage) {
    this.storage.get("_aat").then(aat => {
      this._at = aat;
      this.storage.get("_ap").then(p => {
          this.posts = p;
      });
      if (this._at) {
        this.loggedIn = true;
      }
    });
  }

  // SETTERS
  public authenticate(body : any, cb : Function) {
    this._at = body._tk;
    this.posts = 0;
    this.storage.set("_aat", this._at);
    this.storage.set("_ap", 0);
    this.loggedIn = true;
    cb();
  }

  public setPosts(amount : number) {
    this.posts = amount;
    this.storage.set("_ap", amount);
  }

  public logout() {
    this.loggedIn = false;
    this._at = null;
    this.posts = 0;
    this.storage.remove("_aat");
    this.storage.remove("_ap");
    this.subscription.emit(true);
  }

  public getSubcription() {
    return this.subscription;
  }

  // GETTERS

  public isAuthenticated() {
    return new Promise((resolve, reject) => {
      this.storage.get("_aat").then(aat => {
        this._at = aat;
        if (this._at) {
          this.loggedIn = true;
        }
        resolve(this.loggedIn);
      });
    });
  }

  public getPosts() {
    return this.posts;
  }

  public getAUTH() {
    return this._at;
  }
}
